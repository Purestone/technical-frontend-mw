Purestone's Technical Test (Midweight Frontend)

Requirements:

- In the assets folder, you will find results-layout.psd. That is what we're aiming for.
- In the data folder, you will find sample.json. There, you have the data for the items in the PSD.

1. You will need to create an AJAX call to get the JSON file.
2. Iterate through each item and get the necessary data as displayed on the PSD.
3. Build the page and markup.

Details: 

- Create a media query for under 480px to make each item full width. Test it in 320px as well. That is the only line of code that should be on the media query.
- The elements should respond without breaking the layout. If any data is breaking the layout, you can add a reduction to the font size.

Happy coding!